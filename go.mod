module hartree.stfc.ac.uk/hbaas-server

go 1.14

require (
	github.com/gocarina/gocsv v0.0.0-20200330101823-46266ca37bd3
	github.com/joho/godotenv v1.3.0
	github.com/kevinburke/go-bindata v3.21.0+incompatible // indirect
	github.com/labstack/echo/v4 v4.1.13
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.5.0
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
