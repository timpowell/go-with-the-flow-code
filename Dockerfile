# Builder stage
# =============
FROM golang:latest AS builder

ARG version
ENV VERSION=$version

WORKDIR /app

COPY Makefile .
COPY go.mod .
COPY go.sum .
RUN make download-dependencies

COPY . .

RUN make build-linux

# Runner stage
# ============

FROM alpine

COPY --from=builder /app/hbaas-server /app/

# Required for some cloud providers for remote access.
RUN apk add bash

# Document that our service listens on port 8000
EXPOSE 8000

CMD ["/app/hbaas-server"]